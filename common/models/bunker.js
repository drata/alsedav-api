'use strict';

var _ = require('lodash');

module.exports = function(Bunker) {

	var db = null;

  Bunker.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Bunker.findAllCuts = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllCuts');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Bunker.getBunkerData = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getBunkerData');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Bunker.getCurrentCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getBunkerData');

      //Obtener id del corte activo
      pg.query(query, [], function(err, resp) {
        
        if(err) {
          pg.end();
          return fn(err);
        }

        var cut = _.first(resp.rows);
        var query = require('../../queries/getCutById');

        //Obtener datos de corte actual
        pg.query(query, [cut.id_corte], function(err, resp) {

          if(err) {
            pg.end();
            return fn(err);
          }

          var currentCut =  _.first(resp.rows);

          if(!currentCut) {
            pg.end();
            return fn(null,{});
          }
          
          var query = require('../../queries/getTotalsOfCut');

          //Anexar totales
          pg.query(query, [cut.id_corte], function(err, resp) {
            if(err) {
              pg.end();
              return fn(err);
            }
            pg.end();
            var totals =  _.first(resp.rows);
            currentCut.totals = {
              saldo_caja: totals.tsaldo_caja,
              abonos_caja: totals.tabonos_caja,
              cargos_caja: totals.tcargos_caja,
              cargos: totals.tcargos,
              abonos: totals.tabono,
            }
            return fn(null,currentCut);
          });

        });
      });
    });
  };

  Bunker.saveCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.fecha,
        request.body.saldo_apertura,
        request.body.comentarios,
        request.body.id_usuario
      ];

      var query = require('../../queries/saveCut');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Bunker.findAllMoves = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllMoves');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Bunker.saveMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_empresa,
        request.body.id_sucursal,
        request.body.id_proveedor,
        request.body.concepto,
        request.body.importe,
        request.body.metodo,
        request.body.tipo,
        request.body.movimiento,
        request.body.id_usuario
      ];

      var query = require('../../queries/saveMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Bunker.getCutById = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

        var query = require('../../queries/getCutById');

        //Obtener datos de corte actual
        pg.query(query, [request.params.id], function(err, resp) {

          if(err) {
            pg.end();
            return fn(err);
          }

          var currentCut =  _.first(resp.rows);

          if(!currentCut) {
            pg.end();
            return fn(null,{});
          }

          var query = require('../../queries/getTotalsOfCut');

          //Anexar totales
          pg.query(query, [request.params.id], function(err, resp) {
            if(err) {
              pg.end();
              return fn(err);
            }
            pg.end();
            var totals =  _.first(resp.rows);
            currentCut.totals = {
              saldo_caja: totals.tsaldo_caja,
              abonos_caja: totals.tabonos_caja,
              cargos_caja: totals.tcargos_caja,
              cargos: totals.tcargos,
              abonos: totals.tabono,
            }
            return fn(null,currentCut);
          });//QUERY getTotalsOfCut

        });//QUERY getCutById

    });//DB CONNECT
  };

  Bunker.closingCut = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.saldo_real,
        request.body.comentarios,
        request.body.id_usuario
      ];

      var query = require('../../queries/closingCut');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Bunker.editMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_empresa,
        request.body.id_sucursal,
        request.body.movimiento === 'A' ? 0 : request.body.id_proveedor,
        request.body.concepto,
        request.body.importe,
        request.body.metodo,
        request.body.tipo,
        request.body.movimiento,
        request.body.id_usuario
      ];

      var query = require('../../queries/editMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Bunker.deleteMove = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deleteMove');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Bunker.remoteMethod('deleteMove', {
    http: {verb: 'post', path: '/moves/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Bunker.remoteMethod('editMove', {
    http: {verb: 'post', path: '/moves/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Bunker.remoteMethod('closingCut', {
    http: {verb: 'post', path: '/cuts/close'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Bunker.remoteMethod('getCutById', {
    http: {verb: 'get', path: '/cuts/:id'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cut', type: 'object' }
  });
  
  Bunker.remoteMethod('saveMove', {
    http: {verb: 'post', path: '/moves/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Bunker.remoteMethod('findAllMoves', {
    http: {verb: 'get', path: '/moves'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'moves', type: 'array' }
  });
  
  Bunker.remoteMethod('saveCut', {
    http: {verb: 'post', path: '/cuts/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Bunker.remoteMethod('getCurrentCut', {
    http: {verb: 'get', path: '/cuts/active'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cut', type: 'object' }
  });


  Bunker.remoteMethod('getBunkerData', {
    http: {verb: 'get', path: '/data'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Bunker.remoteMethod('findAllCuts', {
    http: {verb: 'get', path: '/cuts'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'cuts', type: 'array' }
  });

};
