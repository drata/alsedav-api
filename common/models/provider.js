'use strict';

var _ = require('lodash');

module.exports = function(Provider) {

	var db = null;

  Provider.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Provider.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllProviders');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Provider.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'providers', type: 'array' }
  });

};
