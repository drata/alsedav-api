'use strict';
var _ = require('lodash');


module.exports = function(User) {

  var AccessToken = null;

  User.getApp(function(err, app) {
    AccessToken = app.models.AccessToken;
  });

  var defaulLoginFn = User.login;

  User.afterRemote('updateAll', function(context, modelInstance, next) {

    var idUser = context.args.where.id;
    var data_user = context.req.body;
    if (_.hasIn(data_user, 'profile')) {
      AccessToken.destroyAll({ userId: idUser}, function(err,info) {
        if (err) return next(err);   
        return next();
      });
    }
    else{
      next();
    }
    
  });

  User.login = function(credentials, include, fn) {
    defaulLoginFn.call(User, credentials, 'user', function(err, token) {
      if (err) return fn(err); 

      var user = token.__data.user;

      token.__data.user = {
        id: user.id,
        name: user.name,
        last_name: user.last_name,
        username: user.username,
        email: user.email,
        profile: user.profile,
        id_sucursal: user.id_sucursal,
        id_empresa: user.id_empresa
      };

      return fn(null, token);
    });
  };

  User.resetPassword = function(request, fn) {
    User.findById(request.params.id, function(err, user) {
      if (err) return fn(err);
      user.updateAttribute('password', request.query.password, function(err, newData) {
        if (err) return fn(err);
        return fn(null,user);
      });
    });
  };

   User.remoteMethod('resetPassword', {
    http: { verb: 'POST', path: '/:id/reset-password/' },
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: {
      arg: 'res',
      type: 'object'
    }
  });

};
