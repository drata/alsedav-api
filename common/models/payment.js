'use strict';

var _ = require('lodash');

module.exports = function(Payment) {

	var db = null;

  Payment.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Payment.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllPayments');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Payment.save = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_proveedor,
        request.body.id_empresa,
        request.body.id_sucursal,
        request.body.id_usuario,
        request.body.concepto,
        request.body.tipo,
        request.body.importe_solicitado,
        request.body.metodo,
        request.body.fecha_vigencia
      ];

      var query = require('../../queries/savePayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.deletePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.comentarios,
        request.body.id_usuario
      ];
       
      var query = require('../../queries/deletePayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };


  Payment.authorizePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.importe_autorizado,
        request.body.id_usuario
      ];

      var query = require('../../queries/authorizePayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.declinePayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.comentarios,
        request.body.id_usuario
      ];
       
      var query = require('../../queries/declinePayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.finishPayment = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.importe_pagado,
        request.body.id_usuario
      ];

      var query = require('../../queries/finishPayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.getMovements = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getMovementsOfPayment');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Payment.getByUser = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPaymentsByUser');

      pg.query(query, [request.params.userId], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Payment.getPaymentsTypes = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getPaymentsTypes');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Payment.getMethods = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getMethods');

      pg.query(query, [], function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Payment.edit = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_proveedor,
        request.body.importe_solicitado,
        request.body.concepto,
        request.body.tipo,
        request.body.metodo,
        request.body.fecha_vigencia,
        request.body.id_usuario
      ];


      var query = require('../../queries/editPayment');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.saveType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.nombre,
        request.body.movimiento,
        request.body.visible,
        request.body.id_usuario
      ];

      var query = require('../../queries/savePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.updateType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.nombre,
        request.body.movimiento,
        request.body.visible,
        request.body.id_usuario
      ];

      var query = require('../../queries/updatePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Payment.deleteType = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deletePaymentsType');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };
  
  Payment.remoteMethod('deleteType', {
    http: {verb: 'post', path: '/types/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('updateType', {
    http: {verb: 'post', path: '/types/update'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('saveType', {
    http: {verb: 'post', path: '/types'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('edit', {
    http: {verb: 'post', path: '/edit'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Payment.remoteMethod('getMethods', {
    http: {verb: 'get', path: '/methods'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'methods', type: 'array' }
  });

  Payment.remoteMethod('getPaymentsTypes', {
    http: {verb: 'get', path: '/types'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'types', type: 'array' }
  });

  Payment.remoteMethod('getByUser', {
    http: {verb: 'get', path: '/:userId'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'payments', type: 'array' }
  });
  
  Payment.remoteMethod('getMovements', {
    http: {verb: 'get', path: '/:id/movements'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'movements', type: 'array' }
  });

  Payment.remoteMethod('finishPayment', {
    http: {verb: 'post', path: '/finish'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Payment.remoteMethod('declinePayment', {
    http: {verb: 'post', path: '/decline'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  
  Payment.remoteMethod('authorizePayment', {
    http: {verb: 'post', path: '/authorize'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Payment.remoteMethod('deletePayment', {
    http: {verb: 'post', path: '/delete'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Payment.remoteMethod('save', {
    http: {verb: 'post', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });



  Payment.remoteMethod('findAll', {
    http: {verb: 'get', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'payments', type: 'array' }
  });

};
