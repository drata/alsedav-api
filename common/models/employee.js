'use strict';

var _ = require('lodash');

module.exports = function(Employee) {

	var db = null;

  Employee.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Employee.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllEmployees');

      pg.query(query, [], function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Employee.getReportTimer = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.fecha_inicial,
        request.body.fecha_final,
        request.body.empresa,
        request.body.sucursal,
        request.body.empleado
      ];
       
      var query = require('../../queries/reports/getRecordsOfTimer');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        return fn(null, resp.rows);;
      });

    });
  };

  Employee.save = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id_empresa,
        request.body.id_sucursal,
        request.body.nombre,
        request.body.apellido_materno,
        request.body.apellido_paterno,
        request.body.calle,
        request.body.colonia,
        request.body.num_ext,
        request.body.num_int,
        request.body.cod_postal,
        request.body.localidad,
        request.body.municipio,
        request.body.estado,
        request.body.email,
        request.body.telefono,
        request.body.fecha_ingreso,
        request.body.sexo,
        request.body.puesto,
        request.body.id_usuario
      ];

      var query = require('../../queries/saveEmployee');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.edit = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_empresa,
        request.body.id_sucursal,
        request.body.nombre,
        request.body.apellido_materno,
        request.body.apellido_paterno,
        request.body.calle,
        request.body.colonia,
        request.body.num_ext,
        request.body.num_int,
        request.body.cod_postal,
        request.body.localidad,
        request.body.municipio,
        request.body.estado,
        request.body.email,
        request.body.telefono,
        request.body.fecha_ingreso,
        request.body.sexo,
        request.body.puesto,
        request.body.id_usuario
      ];

      var query = require('../../queries/editEmployee');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.deactivate = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deactivateEmployee');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.reactivate = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/reactivateEmployee');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.countEmployees = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/countEmployees');

      pg.query(query,[], function(err, resp) {
        
        if(err) return fn(err);

        var result = _.first(resp.rows);

        var count = {
          total: result.total,
          activos: result.activos,
          bajas: result.bajas
        };


        var query = require('../../queries/countEmployeesByBussiness');

        pg.query(query, [], function(err, resp) {
        
          if(err) return fn(err);
        
          count.empresas = resp.rows;

          
          var query = require('../../queries/countEmployeesByBranches');

          pg.query(query,[] , function(err, resp) {
            pg.end();
          
            if(err) return fn(err);
        
            count.sucursales = resp.rows;

            
            return fn(null, count);
          });
        });
      });

    });
  };

  Employee.getByBranch = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getEmployeesByBranch');

      pg.query(query, [ request.params.branchId], function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Employee.getLogs = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getLogsOfEmployee');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Employee.getKey = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/getKeyOfEmployee');

      var queryParams = [
        request.params.id
      ];

      pg.query(query, queryParams, function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, _.first(resp.rows));
      });
    });
  };

  Employee.activeKey = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.clave,
        request.body.id_usuario
      ];

      var query = require('../../queries/activeKeyEmployee');


      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.deactiveKey = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var queryParams = [
        request.body.id,
        request.body.id_usuario
      ];

      var query = require('../../queries/deactiveKeyEmployee');

      pg.query(query, queryParams, function(err, resp) {
        pg.end();
        if(err) return fn(err);
        var result = _.first(resp.rows);
        return fn(null, result);
      });

    });
  };

  Employee.remoteMethod('deactiveKey', {
    http: {verb: 'post', path: '/key/deactive'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Employee.remoteMethod('activeKey', {
    http: {verb: 'post', path: '/key/active'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Employee.remoteMethod('getKey', {
    http: {verb: 'get', path: '/:id/key'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'key', type: 'object' }
  });
  
  Employee.remoteMethod('getLogs', {
    http: {verb: 'get', path: '/:id/logs'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'logs', type: 'array' }
  });

  Employee.remoteMethod('getByBranch', {
    http: {verb: 'get', path: '/:branchId'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'employees', type: 'array' }
  });

  
  Employee.remoteMethod('countEmployees', {
    http: {verb: 'post', path: '/countEmployees'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Employee.remoteMethod('reactivate', {
    http: {verb: 'post', path: '/reactivate'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Employee.remoteMethod('deactivate', {
    http: {verb: 'post', path: '/deactivate'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Employee.remoteMethod('edit', {
    http: {verb: 'post', path: '/edit'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });
  
  Employee.remoteMethod('save', {
    http: {verb: 'post', path: '/'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });

  Employee.remoteMethod('getReportTimer', {
    http: {verb: 'post', path: '/reports/timer'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'res', type: 'object' }
  });


  Employee.remoteMethod('findAll', {
    http: {verb: 'get', path: '/findAll'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'employees', type: 'array' }
  });

};
