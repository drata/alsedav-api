'use strict';

module.exports = function(Branch) {

	var db = null;

  Branch.getApp(function(err, app) {
    db = app.pgConnection;
  });

	Branch.findAll = function(request, fn) {
    db.connect(function(err, pg) {
      if(err) return fn(err);

      var query = require('../../queries/findAllBranches');

      pg.query(query, [], function(err, resp) {
        pg.end();

        if(err) return fn(err);
        return fn(null, resp.rows);
      });
    });
  };

  Branch.remoteMethod('findAll', {
    http: {verb: 'get', path: '/findAll'},
    accepts: [{
      arg: 'request',
      type: 'object',
      http: function(ctx) {
        return ctx.req;
      }
    }],
    returns: { arg: 'branches', type: 'array' }
  });

};
