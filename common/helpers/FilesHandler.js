var fs = require('fs');
var Q = require('q');
var cdnRootPath = require('path').resolve(__dirname, '../../cdn');

function FilesHandler() {

  var PATHS = {
    'attachments'      : '/attachments/'
  };

  var getDirectories = function (folder) {
    var deferred = Q.defer();
    var path = cdnRootPath + PATHS[folder];
    fs.readdir(path, function (err, content) {
      if (err) return deferred.reject(err);
      return deferred.resolve(content);
    });
    return deferred.promise;
  };
  
  var deleteFile = function(fileName,folder) {
    var deferred = Q.defer();
    var path = cdnRootPath + PATHS[folder] + fileName;
    fs.unlink(path, function(err) {
      if (err) return deferred.reject(err);
      return deferred.resolve();
    });
    return deferred.promise;
  };

  var renameFile = function(fileNameOld,fileNameNew,folder) {
    var deferred = Q.defer();
    var pathOld = cdnRootPath + PATHS[folder] + fileNameOld;
    var pathNew = cdnRootPath + PATHS[folder] + fileNameNew;
    fs.rename(pathOld,pathNew, function(err) {
      if (err) return deferred.reject(err);
      return deferred.resolve();
    });
    return deferred.promise;
  };

  return {
    getDirectories: getDirectories,
    deleteFile: deleteFile,
    renameFile: renameFile
  };
}

module.exports = FilesHandler;
