module.exports='SELECT e.id,e.id_empresa,p.nombre_comercial AS nombre_empresa,e.id_sucursal,s.nombre AS nombre_sucursal, '+
'e.nombre,e.apellido_paterno,e.apellido_materno,e.telefono,e.email,e.calle,e.num_ext,e.num_int,e.colonia, '+
'e.cod_postal,e.localidad,e.municipio,e.estado,e.sexo, '+
'e.puesto,e.estatus,e.fecha_ingreso::text AS fecha_ingreso,e.registro::text AS registro, '+
'CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella,c.clave '+
'FROM empleados AS e  '+
'LEFT JOIN empresas AS p ON(e.id_empresa=p.id) '+
'LEFT JOIN sucursales AS s ON(e.id_sucursal=s.id) '+
'LEFT JOIN huella_empleado AS h ON(e.id = h.id_empleado) '+
'LEFT JOIN clave_checador_empleado AS c ON(e.id = c.id_empleado) '+
'ORDER BY e.id DESC;';