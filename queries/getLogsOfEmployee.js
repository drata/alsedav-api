module.exports="SELECT h.id,h.id_empleado,h.id_usuario,u.name nombre_usuario,u.name||' '||u.last_name AS nombre_completo_usuario, "+
'e.nombre,e.apellido_paterno,e.apellido_materno,h.descripcion,h.tipo,h.registro::text AS registro '+
'FROM historico_empleado AS h '+
'LEFT JOIN empleados AS e ON(h.id_empleado = e.id) '+
'LEFT JOIN users AS u ON(h.id_usuario = u.id) '+
'WHERE id_empleado=$1 ORDER BY id DESC;';