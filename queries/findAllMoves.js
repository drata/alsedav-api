module.exports="SELECT m.id,m.id_corte,m.id_proveedor,COALESCE(p.nombre_comercial,'N/A') AS nombre_proveedor, "+
"m.id_empresa,COALESCE(e.nombre_comercial,'SIN ASIGNAR') AS nombre_empresa,m.metodo,o.nombre AS nombre_metodo, "+
"m.id_sucursal,COALESCE(s.nombre,'SIN ASIGNAR') AS nombre_sucursal,m.id_usuario,u.name nombre_usuario,u.name||' '||u.last_name AS nombre_completo_usuario,m.concepto, "+
'm.importe,m.tipo,t.nombre AS tipo_pago,m.movimiento,m.estatus,m.fecha::text AS fecha,m.registro::text AS registro '+
'FROM movimientos AS m '+
'LEFT JOIN cat_tipos_pagos AS t ON(m.tipo = t.id) ' +
'LEFT JOIN cat_metodos_pago AS o ON(m.metodo = o.id) '+
'LEFT JOIN proveedores AS p ON(m.id_proveedor = p.id) '+
'LEFT JOIN empresas AS e ON(m.id_empresa = e.id) '+
'LEFT JOIN sucursales AS s ON(m.id_sucursal = s.id) '+
'LEFT JOIN users AS u ON(m.id_usuario = u.id) '+
"WHERE m.estatus IN ('R','P') "+
'ORDER BY m.id DESC;';
