module.exports = 'SELECT s.id,s.nombre,s.id_empresa,e.nombre_comercial AS nombre_empresa,s.nombre,s.telefono,s.email,s.calle, '+
's.num_ext,s.num_int,s.colonia,s.cod_postal,s.localidad,s.localidad,s.municipio,s.estado, '+
's.fecha_apertura::text AS fecha_apertura,s.estatus, s.registro::text AS registro '+
'FROM sucursales AS s '+
'LEFT JOIN empresas AS e ON(s.id_empresa=e.id) ORDER BY id;';
