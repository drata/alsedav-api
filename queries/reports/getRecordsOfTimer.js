module.exports = 'SELECT fecha::text AS fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado, '+
'entrada1::text AS entrada1,salida1::text AS salida1,entrada2::text AS entrada2,salida2::text AS salida2,num,huella,incidencias '+
'FROM reporte_checador($1,$2,$3,$4,$5) '+
'AS (fecha date ,id_empresa integer,id_sucursal integer,id_empleado integer,nombre_empleado text, '+
'entrada1 timestamp,salida1 timestamp,entrada2 timestamp,salida2 timestamp,num integer,huella boolean,incidencias jsonb);';
