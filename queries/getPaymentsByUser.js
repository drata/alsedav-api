module.exports="SELECT p.id,p.id_proveedor,pv.nombre_comercial AS nombre_proveedor,p.id_empresa,COALESCE(e.nombre_comercial,'SIN ASIGNAR') AS nombre_empresa,p.id_sucursal,COALESCE(s.nombre,'SIN ASIGNAR') AS nombre_sucursal, "+
"p.id_usuario,u.name||' '||u.last_name AS nombre_usuario,u.name,p.concepto,p.tipo,p.importe_solicitado,p.fecha_vigencia::text AS fecha_vigencia, "+
'p.metodo,p.importe_autorizado,p.fecha_autorizado::text AS fecha_autorizado,p.fecha_pago::text AS fecha_pago,p.importe_pagado,p.estatus,p.motivo_rechazo,p.registro::text AS registro '+
'FROM solicitud_pagos AS p '+
'LEFT JOIN proveedores AS pv ON(p.id_proveedor = pv.id)'+
'LEFT JOIN empresas AS e ON(p.id_empresa = e.id) '+
'LEFT JOIN sucursales AS s ON(p.id_sucursal = s.id) '+
'LEFT JOIN users AS u ON (p.id_usuario = u.id) '+
'WHERE id_usuario = $1 '+
'ORDER BY id DESC;';