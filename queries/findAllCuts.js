module.exports="SELECT c.id,c.id_usuario,u.name||' '||u.last_name AS nombre_usuario,c.saldo_apertura, "+
'c.saldo_cierre,c.diferencia,c.estatus,c.fecha::text AS fecha,c.registro::text AS registro '+
'FROM cortes AS c '+
'LEFT JOIN users AS u ON (c.id_usuario = u.id)' +
'ORDER BY c.id DESC;';