function errorConnectionToDatabase(message) {
  this.name = 'errorConnectionToDatabase';
  this.message = message || 'Error, al conectarse a la base de datos';
  this.statusCode = 503;
}

errorConnectionToDatabase.prototype = Object.create(Error.prototype);
errorConnectionToDatabase.prototype.constructor = errorConnectionToDatabase;

module.exports = errorConnectionToDatabase;