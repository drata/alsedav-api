var express = require('express');

module.exports = function(server) {
  server.use('/attachments', express.static('cdn/attachments/'));
};