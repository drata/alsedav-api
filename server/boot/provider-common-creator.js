var _ = require('lodash');

module.exports = function(app) {

  var db = app.pgConnection; 

  db.connect(function(err, pg) {
    if(err) throw err;
     
    var query = require('../../queries/createProviderDefault');

    pg.query(query, [], function(err, resp) {
      pg.end();
      if(err) throw err;
      var result = _.first(resp.rows);
      if (result.flag) 
        console.log(result.mensaje);
    });

  });

};