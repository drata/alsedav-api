var databases = require('./db-config');
var pg = require('pg');

var errorConnectionToDatabase = require('../errors/errorConnectionToDatabase');

function PgConnector() {
  var self = this;

  this.connectToBranchByRequest = function(request, cb) {
    return self.connectToBranch(request.headers.branch, cb);
  };

  this.connectToBranch = function(branchId, cb) {
    var db = databases[branchId];
    if(!db) return cb(new errorConnectionToDatabase());
    var connectionString = 'postgres://' + (db.username || 'postgres') + ':' +
                           db.password + '@' + db.ip + '/' + db.database;

    var client = new pg.Client(connectionString);

    client.connect(function(err) {
      if(err) client.end();

      cb(err, client);
    });
  };

  this.connect = function(cb) {
    var db = databases['grupo-alsedav'];

    if(!db) return cb(new errorConnectionToDatabase());
    var connectionString = 'postgres://' + (db.username || 'postgres') + ':' +
                           db.password + '@' + db.ip + '/' + db.database;

    var client = new pg.Client(connectionString);

    client.connect(function(err) {
      if(err) client.end();

      cb(err, client);
    });
  };

  
}

module.exports = new PgConnector();