CREATE TABLE users
(
  id serial PRIMARY KEY NOT NULL,
  username character varying(32) NOT NULL,
  password text NOT NULL,
  name character varying(100) NOT NULL,
  last_name character varying(100) NOT NULL,
  email character varying(100),
  id_empresa integer REFERENCES empresas(id) NOT NULL,
  id_sucursal integer REFERENCES sucursales(id),
  profile character varying(30) NOT NULL,
  verificationtoken text,
  register timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE check_registros (
  id serial PRIMARY KEY NOT NULL,
  id_empleado integer DEFAULT NULL,
  entrada1 timestamp NULL DEFAULT NULL,
  salida1 timestamp NULL DEFAULT NULL,
  entrada2 timestamp NULL DEFAULT NULL,
  salida2 timestamp NULL DEFAULT NULL,
  num integer DEFAULT 0,
  registro timestamp NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE check_huellasemp (
  id_usuario integer NOT NULL,
  nombre character varying(100) DEFAULT NULL,
  huella bytea,
  departamento character varying(50) NOT NULL DEFAULT ' '
);

CREATE TABLE empresas ( 
  id serial PRIMARY KEY NOT NULL,
  nombre_comercial character varying(200) NOT NULL,
  nombre_fiscal character varying(200) NOT NULL,
  tipo_fiscal character(1) DEFAULT 'F',
  telefono character varying(10),
  email character varying(100),
  calle character varying(100),
  num_ext character varying(10),
  num_int character varying(10),
  colonia character varying(100),
  cod_postal character varying(10),
  localidad character varying(50),
  municipio character varying(50),
  estado character varying(50),
  fecha_apertura date, 
  estatus boolean NOT NULL DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE sucursales ( 
  id serial PRIMARY KEY NOT NULL,
  id_empresa integer REFERENCES empresas(id) NOT NULL,
  nombre character varying(200) NOT NULL,
  telefono character varying(10),
  email character varying(100),
  calle character varying(100),
  num_ext character varying(10),
  num_int character varying(10),
  colonia character varying(100),
  cod_postal character varying(10),
  localidad character varying(50),
  municipio character varying(50),
  estado character varying(50),
  fecha_apertura date, 
  estatus boolean NOT NULL DEFAULT true,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

/*
Estados 
A - Activo
B - Baja
E- Eliminar
R- Reactivacion
*/

CREATE TABLE empleados (
  id serial PRIMARY KEY NOT NULL,
  id_empresa integer REFERENCES empresas(id) NOT NULL,
  id_sucursal integer REFERENCES sucursales(id),
  nombre character varying(200) NOT NULL,
  apellido_paterno character varying(200) NOT NULL,
  apellido_materno character varying(200) ,
  telefono character varying(10),
  email character varying(100),
  calle character varying(100),
  num_ext character varying(10),
  num_int character varying(10),
  colonia character varying(100),
  cod_postal character varying(10),
  localidad character varying(50),
  municipio character varying(50),
  estado character varying(50),
  sexo character(1),
  puesto character varying(50),
  estatus character(1) NOT NULL DEFAULT 'A',
  fecha_ingreso date, 
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);



/*
  A - Alta
  B - Baja
  D - Actualizacion de datos
*/

CREATE TABLE historico_empleado (
  id serial PRIMARY KEY NOT NULL,
  id_empleado integer REFERENCES empleados(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);



/*
S - Solicitado
R - Rechazado
B - Borrado
A - Autorizado
P - Pagado

Atrasado por direccion - Cuando estatus = 'S' y la fecha actual es mayor a fecha_vigencia;
Atrasado por falta de pago - Cuando estatus = 'A' y la fecha actual es mayor a fecha_pago;
*/

CREATE TABLE pagos(
  id serial PRIMARY KEY NOT NULL,
  id_empresa integer REFERENCES empresas(id) NOT NULL,
  id_sucursal integer REFERENCES sucursales(id) NOT NULL,
  id_usuario integer REFERENCES users(id) NOT NULL,
  concepto character varying(300) NOT NULL,
  tipo integer REFERENCES cat_pagos(id) NOT NULL,
  tipo_otro character varying(300),
  importe_solicitado numeric(10,2) NOT NULL,
  fecha_vigencia date NOT NULL, 
  importe_autorizado numeric(10,2),
  fecha_autorizado timestamp without time zone,
  importe_pagado numeric(10,2),
  fecha_pago  timestamp without time zone, 
  estatus character(1) NOT NULL DEFAULT 'S',
  motivo_rechazo character varying(300),
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE cat_pagos(
  id serial PRIMARY KEY NOT NULL,
  nombre character varying(300),
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

INSERT INTO cat_pagos(nombre) VALUES('OTROS');
INSERT INTO cat_pagos(nombre) VALUES('GASTOS GENERALES');
INSERT INTO cat_pagos(nombre) VALUES('PAGO SERVICIOS');
INSERT INTO cat_pagos(nombre) VALUES('PRESTAMO PERSONAL');
INSERT INTO cat_pagos(nombre) VALUES('FINIQUITO');
INSERT INTO cat_pagos(nombre) VALUES('INSUMOS OFICINA');
INSERT INTO cat_pagos(nombre) VALUES('REDES SOCIALES');
INSERT INTO cat_pagos(nombre) VALUES('PATROCINIOS');


CREATE TABLE logs_pagos (
  id serial PRIMARY KEY NOT NULL,
  id_pago integer REFERENCES pagos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE cat_tipos_pagos
(
 id serial PRIMARY KEY,
 nombre character varying(300),
 flag boolean DEFAULT true,
 registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


INSERT INTO cat_tipos_pagos(nombre) VALUES('GASTOS EN GENERAL');
INSERT INTO cat_tipos_pagos(nombre) VALUES('INSUMOS');
INSERT INTO cat_tipos_pagos(nombre) VALUES('COMPRAS DE TECNOLGIA');



CREATE TABLE proveedores
(
  id serial PRIMARY KEY,
  id_empresa integer NOT NULL DEFAULT 0,
  id_tipo_pago integer NOT NULL DEFAULT 0,
  rfc character(15) NOT NULL,
  nombre character varying(100) NOT NULL,
  nombre_comercial character varying(100) NOT NULL,
  email character varying(100) NOT NULL,
  telefono character varying(10) NOT NULL,
  calle character varying(100) NOT NULL,
  num_ext integer NOT NULL,
  num_int integer DEFAULT 0,
  colonia character varying(100) NOT NULL,
  cod_postal character varying(10) NOT NULL,
  localidad character varying(50) NOT NULL,
  municipio character varying(50) NOT NULL,
  estado character varying(50) NOT NULL,
  credito numeric(10,2) DEFAULT 0.00,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
  flag boolean DEFAULT true
);