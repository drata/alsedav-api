﻿SELECT e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno as nombre,c.entrada1,c.salida1,c.entrada2,c.salida2 
FROM check_registros c 
INNER JOIN empleados e ON(c.id_empleado = e.id) 
WHERE entrada1::date = '2018-09-07'
ORDER BY nombre, entrada1;

CREATE OR REPLACE FUNCTION reporte_checador(
    ifecha_inicial date,
    ifecha_final date,
    iempresa integer,
    isucursal integer,
    iempleado integer)
  RETURNS SETOF record AS
$BODY$
DECLARE

	registros record;
	linea_empresa record;
	linea_sucursal record;
	rango record;

BEGIN

	DROP TABLE IF EXISTS reporte_checador;
	DROP TABLE IF EXISTS rango_fechas;
	
	CREATE TEMP TABLE rango_fechas (
		fecha date
	);

	CREATE TEMP TABLE reporte_checador (
	    fecha date,
	    id_empresa integer,
	    id_sucursal integer,
	    id_empleado integer,
	    nombre_empleado text,
	    entrada1 timestamp,
	    salida1 timestamp,
	    entrada2 timestamp,
	    salida2 timestamp,
	    num integer,
	    huella boolean
	);

	INSERT INTO rango_fechas(fecha)
	SELECT generate_series(ifecha_inicial::date,ifecha_final::date,'1 day')::date AS fecha;

	IF iempleado = 0 THEN

		IF iempresa = 0 THEN

			FOR linea_empresa IN SELECT id,nombre_comercial AS nombre FROM empresas LOOP 

				IF isucursal = 0 THEN

					FOR linea_sucursal IN SELECT id,nombre FROM sucursales LOOP 
						FOR rango IN SELECT fecha fROM rango_fechas LOOP
							INSERT INTO reporte_checador(fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella)
							SELECT COALESCE(c.registro::date,rango.fecha) AS fecha,e.id_empresa,e.id_sucursal,e.id AS id_empleado,
							(e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno ) as nombre_empleado,
							c.entrada1,c.salida1,c.entrada2,c.salida2,c.num,
							CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella
							FROM empleados AS e
							LEFT JOIN check_registros as c ON(e.id = c.id_empleado AND c.registro::date = rango.fecha)
							LEFT JOIN check_huellasemp AS h ON(h.id_usuario = e.id)
							WHERE e.id_empresa = linea_empresa.id AND e.id_sucursal = linea_sucursal.id AND e.estatus = 'A'
							ORDER BY e.nombre;
						END LOOP;
					END LOOP; -- LOOP Sucursales

				ELSE
					FOR rango IN SELECT fecha fROM rango_fechas LOOP
						INSERT INTO reporte_checador(fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella)
						SELECT COALESCE(c.registro::date,rango.fecha) AS fecha,e.id_empresa,e.id_sucursal,e.id AS id_empleado,
						(e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno ) as nombre_empleado,
						c.entrada1,c.salida1,c.entrada2,c.salida2,c.num,
						CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella
						FROM empleados AS e
						LEFT JOIN check_registros as c ON(e.id = c.id_empleado AND c.registro::date = rango.fecha)
						LEFT JOIN check_huellasemp AS h ON(h.id_usuario = e.id)
						WHERE e.id_empresa = linea_empresa.id AND e.id_sucursal = isucursal AND e.estatus = 'A'
						ORDER BY e.nombre;
					END LOOP;
				END IF; --IF sucursal
			
			END LOOP; -- LOOP EMPRESAS

		ELSE
			IF isucursal = 0 THEN

				FOR linea_sucursal IN SELECT id,nombre FROM sucursales LOOP 
					FOR rango IN SELECT fecha fROM rango_fechas LOOP
						INSERT INTO reporte_checador(fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella)
						SELECT COALESCE(c.registro::date,rango.fecha) AS fecha,e.id_empresa,e.id_sucursal,e.id AS id_empleado,
						(e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno ) as nombre_empleado,
						c.entrada1,c.salida1,c.entrada2,c.salida2,c.num,
						CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella
						FROM empleados AS e
						LEFT JOIN check_registros as c ON(e.id = c.id_empleado AND c.registro::date = rango.fecha)
						LEFT JOIN check_huellasemp AS h ON(h.id_usuario = e.id)
						WHERE e.id_empresa = iempresa AND e.id_sucursal = linea_sucursal.id AND e.estatus = 'A'
						ORDER BY e.nombre;
					END LOOP;
				END LOOP; -- LOOP Sucursales

			ELSE
				FOR rango IN SELECT fecha fROM rango_fechas LOOP
					INSERT INTO reporte_checador(fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella)
					SELECT COALESCE(c.registro::date,rango.fecha) AS fecha,e.id_empresa,e.id_sucursal,e.id AS id_empleado,
					(e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno ) as nombre_empleado,
					c.entrada1,c.salida1,c.entrada2,c.salida2,c.num,
					CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella
					FROM empleados AS e
					LEFT JOIN check_registros as c ON(e.id = c.id_empleado AND c.registro::date = rango.fecha)
					LEFT JOIN check_huellasemp AS h ON(h.id_usuario = e.id)
					WHERE e.id_empresa = iempresa AND e.id_sucursal = isucursal AND e.estatus = 'A'
					ORDER BY e.nombre;
				END LOOP;
			END IF; --IF sucursal
				
		END IF; -- IF empresa
		
	ELSE
		FOR rango IN SELECT fecha fROM rango_fechas LOOP
			INSERT INTO reporte_checador(fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella)
			SELECT COALESCE(c.registro::date,rango.fecha) AS fecha,e.id_empresa,e.id_sucursal,e.id AS id_empleado,
			(e.nombre || ' ' || e.apellido_paterno || ' ' || e.apellido_materno ) as nombre_empleado,
			c.entrada1,c.salida1,c.entrada2,c.salida2,c.num,
			CASE WHEN h.huella IS NULL THEN false ELSE true END AS huella
			FROM empleados AS e
			LEFT JOIN check_registros as c ON(e.id = c.id_empleado AND c.registro::date = rango.fecha)
			LEFT JOIN check_huellasemp AS h ON(h.id_usuario = e.id)
			WHERE e.id = iempleado AND e.estatus = 'A';
		END LOOP;
	
	END IF;

	FOR registros IN SELECT fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella FROM reporte_checador  LOOP
		RETURN NEXT registros;
	END LOOP;

	RETURN; 

END
$BODY$
  LANGUAGE plpgsql VOLATILE;


SELECT fecha,id_empresa,id_sucursal,id_empleado,nombre_empleado,entrada1,salida1,entrada2,salida2,num,huella
FROM reporte_checador('2018-09-11','2018-09-11',1,0,0)
AS (fecha date ,id_empresa integer,id_sucursal integer,id_empleado integer,nombre_empleado text,
entrada1 timestamp,salida1 timestamp,entrada2 timestamp,salida2 timestamp,num integer,huella boolean);



CREATE OR REPLACE FUNCTION public.generar_checada(
    iempleado integer,
    iregistro timestamp without time zone)
  RETURNS SETOF record AS
$BODY$
DECLARE

registros record;
minutos_transcurridos integer;
registro_actual timestamp;
contador integer;
num_checada integer;

BEGIN
          
  --Contabilizar registros de checadas del empleado
  SELECT count(id_empleado) FROM check_registros WHERE id_empleado = iempleado AND registro = date(iregistro) INTO contador;
  
  --Extraemos la diferencia de minutos transcurridos entre la ultimachecada de hoy y esta que esta haciendo el empleado
  --Si aun no han pasado 25 minutos despues de la ultima checada no lo deja checar y le devuelve la anterior

  IF contador = 0 THEN

    INSERT INTO check_registros(id_empleado,entrada1,num,registro) VALUES(iempleado,iregistro,1,date(iregistro));     
  ELSE
    SELECT num,
    CASE WHEN num = 1 THEN entrada1 
         WHEN num = 2 THEN salida1
         WHEN num = 3 THEN entrada2
    ELSE
        salida2 
    END 
    INTO num_checada,registro_actual FROM check_registros WHERE id_empleado=iempleado AND registro=date(iregistro);

    IF num_checada < 4 THEN 

      SELECT extract(epoch FROM age(checo,registro_actual))/60 INTO minutos_transcurridos;                        
        
      IF minutos_transcurridos > 25 then
         
        IF num_checada = 1 then
           num_checada := num_checada + 1;
           UPDATE check_registros SET salida1=checo,num=num_checada WHERE id_empleado=iempleado AND registro=date(iregistro);         
           
        ELSE  
          IF num_checada = 2 then
            num_checada := num_checada + 1;
            UPDATE check_registros SET entrada2=iregistro,num=num_checada WHERE id_empleado=iempleado AND registro=date(iregistro);         
          ELSE
            num_checada := num_checada + 1;
            UPDATE check_registros SET salida2=iregistro,num=num_checada WHERE id_empleado=iempleado AND registro=date(iregistro);        
              
          END IF; -- num_checada = 2
          
        END IF; -- num_checada = 1

      END IF; -- minutos_transcurridos > 25                 

    END IF; --num_cheada < 4

  END IF; --CONTADOR 0
      

    
  
  FOR registros IN SELECT registro,entrada1,salida1,entrada2,salida2 FROM check_registros WHERE id_empleado=iempleado AND registro=date(iregistro) ORDER BY registro ASC LOOP
      RETURN NEXT registros;
  END LOOP;
  
  RETURN ; 


END
$BODY$
  LANGUAGE plpgsql VOLATILE;

