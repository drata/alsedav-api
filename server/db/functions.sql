
CREATE OR REPLACE FUNCTION eliminar_tipo_pago(
    IN itipo integer,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-04
-- Descripcion General: Eliminar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar tipo de pago';

  UPDATE cat_tipos_pagos
  SET flag=false
  WHERE id=itipo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(itipo,iusuario,'Elimino el tipo de pago' ,'D');

  flag       := true;
  mensaje    := 'Se elimino correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION agregar_tipo_pago(
    IN inombre character varying,
    IN imovimiento character(1),
    IN ivisible boolean,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-04
-- Descripcion General: Agregar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE
id_tipo_nuevo integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al agregar tipo de pago';

  INSERT INTO cat_tipos_pagos(nombre,visible,movimiento)
  VALUES (inombre,ivisible,imovimiento)
  RETURNING id INTO id_tipo_nuevo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(id_tipo_nuevo,iusuario,'Agrego el tipo de pago: '||inombre,'C');

  flag       := true;
  mensaje    := 'Se agrego correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


  
CREATE OR REPLACE FUNCTION editar_tipo_pago(
    IN itipo integer,
    IN inombre character varying,
    IN imovimiento character(1),
    IN ivisible boolean,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-04
-- Descripcion General: Editar registro a cat_tipos_pagos
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al actualizar tipo de pago';

  UPDATE cat_tipos_pagos
  SET nombre=inombre, visible=ivisible, movimiento=imovimiento
  WHERE id=itipo;

  INSERT INTO logs_tipos_pagos(id_tipo,id_usuario,descripcion,tipo)
  VALUES(itipo,iusuario,'Edito el tipo de pago','U');

  flag       := true;
  mensaje    := 'Se actualizo correctamente el tipo de pago';
  RETURN NEXT;
  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

CREATE OR REPLACE FUNCTION generar_baja_empleado(
  IN iempleado integer,
  IN iusuario integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: BAJA registro a empleados
*****************************************************************************/ 

DECLARE


BEGIN
  --Variables de salida
  flag        := false;
  mensaje     := 'Error al generar baja de empleado ';

  --Se actualiza el empleado
  UPDATE empleados SET estatus='B'
  WHERE id=iempleado;
  
  --Se genera log
  INSERT INTO historico_empleado(id_empleado,id_usuario,descripcion,tipo) 
  VALUES(iempleado,iusuario,'Baja empleado','B');

  flag        := true;
  mensaje     := 'Se genero la baja del empleado correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag        := false;
  mensaje     := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


  

CREATE OR REPLACE FUNCTION generar_reactivacion_empleado(
  IN iempleado integer,
  IN iusuario integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: Reactivacion registro a empleados
*****************************************************************************/ 

DECLARE


BEGIN
  --Variables de salida
  flag        := false;
  mensaje     := 'Error al reactivar empleado ';

  --Se actualiza el empleado
  UPDATE empleados SET estatus='A'
  WHERE id=iempleado;
  
  --Se genera log
  INSERT INTO historico_empleado(id_empleado,id_usuario,descripcion,tipo) 
  VALUES(iempleado,iusuario,'Reactivación empleado','R');

  flag        := true;
  mensaje     := 'Se genero la reactivación del empleado correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag        := false;
  mensaje     := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 





CREATE OR REPLACE FUNCTION editar_empleado(
        IN iempleado integer,
  IN iempresa integer,
  IN isucursal integer,
  IN inombre character varying,
  IN iapellido_materno character varying,
  IN iapellido_paterno character varying,
  IN icalle character varying,
  IN icolonia character varying,
  IN inum_ext character varying,
  IN inum_int character varying,
  IN icod_postal character varying,
  IN ilocalidad character varying,
  IN imunicipio character varying,
  IN iestado character varying,
  IN iemail character varying,
  IN itelefono character varying,
  IN ifecha_ingreso date,
  IN isexo character(1),
  IN ipuesto character varying,
  IN iusuario integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: Editar registro a empleados
*****************************************************************************/ 

DECLARE


BEGIN
  --Variables de salida
  flag        := false;
  mensaje     := 'Error al editar empleado ';

  --Se actualiza el empleado
  UPDATE empleados SET id_empresa=iempresa,id_sucursal=isucursal,nombre=inombre,apellido_materno=iapellido_materno,
    apellido_paterno=iapellido_paterno,calle=icalle,colonia=icolonia,num_ext=inum_ext,
    num_int=inum_int,cod_postal=icod_postal,localidad=ilocalidad,municipio=imunicipio,estado=iestado,
    email=iemail,telefono=itelefono,fecha_ingreso=ifecha_ingreso,sexo=isexo,puesto=ipuesto
  WHERE id=iempleado;
  
  --Se genera log
  INSERT INTO historico_empleado(id_empleado,id_usuario,descripcion,tipo) 
  VALUES(iempleado,iusuario,'Acutalización de datos','D');

  flag        := true;
  mensaje     := 'Se edito el empleado correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag        := false;
  mensaje     := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 



CREATE OR REPLACE FUNCTION agregar_empleado(
  IN iempresa integer,
  IN isucursal integer,
  IN inombre character varying,
  IN iapellido_materno character varying,
  IN iapellido_paterno character varying,
  IN icalle character varying,
  IN icolonia character varying,
  IN inum_ext character varying,
  IN inum_int character varying,
  IN icod_postal character varying,
  IN ilocalidad character varying,
  IN imunicipio character varying,
  IN iestado character varying,
  IN iemail character varying,
  IN itelefono character varying,
  IN ifecha_ingreso date,
  IN isexo character(1),
  IN ipuesto character varying,
  IN iusuario integer,
  OUT id_empleado integer,
  OUT flag boolean,
  OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-09-14
-- Descripcion General: Agregar registro a empleados
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_empleado_nuevo integer;

BEGIN
  --Variables de salida
  id_empleado := 0;
  flag        := false;
  mensaje     := 'Error al guardar empleado ';

  --Se inserta el empleado
  INSERT INTO empleados(id_empresa,id_sucursal,nombre,apellido_materno,apellido_paterno,calle,colonia,num_ext,
            num_int,cod_postal,localidad,municipio,estado,email,telefono,fecha_ingreso,sexo,puesto)
  VALUES(iempresa,isucursal,inombre,iapellido_materno,iapellido_paterno,icalle,icolonia,inum_ext,
            inum_int,icod_postal,ilocalidad,imunicipio,iestado,iemail,itelefono,ifecha_ingreso,isexo,ipuesto)
  RETURNING id INTO id_empleado_nuevo;
  
  --Se genera log
  INSERT INTO historico_empleado(id_empleado,id_usuario,descripcion,tipo) 
  VALUES(id_empleado_nuevo,iusuario,'Alta empleado #'||id_empleado_nuevo,'A');

  id_empleado := id_empleado_nuevo;
  flag        := true;
  mensaje     := 'Se guardo el empleado correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_empleado := 0;
  flag        := false;
  mensaje     := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 



CREATE OR REPLACE FUNCTION editar_solicitud_pago(
    IN isolicitud integer,
    IN iproveedor integer,
    IN iimporte numeric,
    IN iconcepto character varying,
    IN itipo integer,
    IN imetodo integer,
    IN ifecha_vigencia date,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-10
-- Descripcion General: Editar registro a solicitud_pagos
*****************************************************************************/ 

DECLARE

--Variables de traba

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al editar solicitud de pago ';

  --Editar solicitud pago
    
  UPDATE solicitud_pagos 
  SET id_proveedor=iproveedor, importe_solicitado=iimporte, concepto=iconcepto, tipo=itipo, metodo=imetodo, fecha_vigencia=ifecha_vigencia
  WHERE id=isolicitud;  

  --Se genera log
  INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
  VALUES(isolicitud,iusuario,'Edito la solicitud por la cantidad de $'||iimporte||' para el día: '||ifecha_vigencia,'U');

  flag       := true;
  mensaje    := 'Se edito la solicitud de pago correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 
  
  
CREATE OR REPLACE FUNCTION cerrar_corte(
    IN icorte integer,
    IN isaldo_real numeric(10,2),
    IN icomentarios character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-037
-- Descripcion General: Cerar corte
*****************************************************************************/ 

DECLARE

--Variables de trabajo
comentarios_dif character varying;
diferencia_corte numeric(10,2);
dif_corte_nuevo numeric(10,2);
tipo_mov character(1);
saldo_caja numeric(10,2);
saldo_final_corte numeric(10,2);
corte_actual RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al generar ciere de corte';

  --Obtener datos del corte actual
  SELECT saldo_apertura,estatus,fecha INTO corte_actual FROM cortes WHERE id=icorte;

  --Verificar que el corte no tenga un estatus CERRADO
  IF corte_actual.estatus = 'C' THEN
    flag       := true;
    mensaje    := 'El corte seleccionado ya fue cerrado';
    RETURN NEXT;
    RETURN;
  END IF;

  --Obtener el saldo actual del corte
  SELECT tsaldo_caja INTO saldo_caja FROM obtener_montos_corte(icorte);

  diferencia_corte := isaldo_real - saldo_caja;

  IF diferencia_corte = 0 THEN
    saldo_final_corte:= isaldo_real;

  ELSEIF diferencia_corte > 0 THEN

    INSERT INTO movimientos(id_corte,id_usuario,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(icorte,iusuario,'ABONO POR SOBRANTE EN CIERRE',diferencia_corte,1,3,'A','R',corte_actual.fecha);
    
    saldo_final_corte:= (SELECT tsaldo_caja FROM obtener_montos_corte(icorte));

    INSERT INTO incidencias_cierre_corte(id_corte,id_usuario,motivo,movimiento,importe) 
    VALUES(icorte,iusuario,icomentarios,'A',diferencia_corte);

  ELSEIF diferencia_corte < 0 THEN
    
    dif_corte_nuevo:= diferencia_corte * -1;
    INSERT INTO movimientos(id_corte,id_usuario,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(icorte,iusuario,'CARGO POR FALTANTE EN CIERRE',dif_corte_nuevo,1,2,'C','R',corte_actual.fecha);
    
    saldo_final_corte:= (SELECT tsaldo_caja FROM obtener_montos_corte(icorte));

    INSERT INTO incidencias_cierre_corte(id_corte,id_usuario,motivo,movimiento,importe) 
    VALUES(icorte,iusuario,icomentarios,'C',dif_corte_nuevo);

  ELSE 
    flag       := false;
    mensaje    := 'Error al generar ciere de corte';
    RETURN NEXT;
    RETURN;
  END IF;

  --Se genera el cierre
  UPDATE cortes 
  SET estatus='C', 
    saldo_cierre=isaldo_real, 
    diferencia=diferencia_corte, 
    saldo_final=saldo_final_corte 
  WHERE id=icorte;

  --Insertar log
  INSERT INTO logs_cortes(id_corte,id_usuario,descripcion,tipo)
  VALUES (icorte,iusuario,'Realizo el cierre del corte ID: '||icorte,'F');

  flag       := true;
  mensaje    := 'Se realizo correctamente el cierre';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
    flag       := false;
    mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


CREATE OR REPLACE FUNCTION public.obtener_montos_corte(
    IN icorte integer,
    OUT tsaldo_caja numeric,
    OUT tabonos_caja numeric,
    OUT tcargos_caja numeric,
    OUT tcargos numeric,
    OUT tabono numeric,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-05
-- Descripcion General: Obtener montos del corte
*****************************************************************************/ 

DECLARE
saldo_corte numeric;

BEGIN

  --Inicializar variables de salida
  tabonos_caja := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='A' AND metodo=1 AND id_corte=icorte);
  tcargos_caja := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='C' AND metodo=1 AND id_corte=icorte);
  tabono       := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='A' AND id_corte=icorte);
  tcargos      := (SELECT COALESCE(sum(importe),0) FROM movimientos WHERE movimiento='C' AND id_corte=icorte);
  
  IF (SELECT estatus FROM cortes WHERE id=icorte) = 'A' THEN
    saldo_corte := (((SELECT saldo_apertura FROM cortes WHERE id=icorte) + tabonos_caja ) - tcargos_caja);
  ELSE
    saldo_corte := (SELECT saldo_final FROM cortes WHERE id=icorte);
  END IF;
  
  tsaldo_caja  := COALESCE(saldo_corte,0);
  flag       := true;
  mensaje      := 'Se obtuvo correctamente la informacion';  
  RETURN NEXT; 

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
    tsaldo_caja  := 0;
    tabonos_caja := 0;
    tcargos_caja := 0;
    tcargos      := 0;
    tabono       := 0;
    flag       := false;
    mensaje      := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION agregar_movimiento(
    IN iempresa integer,
    IN isucursal integer,
    IN iproveedor integer,
    IN iconcepto character varying,
    IN iimporte numeric(10,2),
    IN imetodo integer,
    IN itipo integer,
    IN imovimiento character(1),
    IN iusuario integer,
    OUT id_movimiento integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-05
-- Descripcion General: Agregar registro a movimientos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_movimiento_nuevo integer;
data_corte RECORD;
fecha_servidor date;
mov text;
  
BEGIN
  --Variables de salida
  id_movimiento := 0;
  flag          := false;
  mensaje       := 'Error al guardar movimiento ';

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;
  SELECT z.fecha_corte,z.id_corte,z.estado,z.mensaje INTO data_corte FROM obtener_corte_actual() AS z;

  IF data_corte.estado = 'A' THEN
  
    --Se inserta movimiento
    INSERT INTO movimientos(id_corte,id_proveedor,id_empresa,id_sucursal,id_usuario,concepto,importe,metodo,tipo,movimiento,estatus,fecha)
    VALUES(data_corte.id_corte,iproveedor,iempresa,isucursal,iusuario,iconcepto,iimporte,imetodo,itipo,imovimiento,'R',fecha_servidor)
    RETURNING id INTO id_movimiento_nuevo;

    IF imovimiento ='A' THEN
      mov:= 'abono';
    ELSE
      mov:= 'pago';
    END IF;

    --Se genera log
    INSERT INTO logs_movimientos(id_movimiento,id_usuario,descripcion,tipo)
    VALUES(id_movimiento_nuevo,iusuario,'Agrego un '||mov,'C');

    id_movimiento := id_movimiento_nuevo;
    flag          := true;
    mensaje       := 'Se guardo el movimiento correctamente';
    RETURN NEXT;
  ELSE
    id_movimiento := 0;
    flag          := false;
    mensaje       := 'Actualmente no hay un corte activo, es necesario aperturar caja para autorizar pago';
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_movimiento   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 
  
CREATE OR REPLACE FUNCTION public.agregar_corte(
    IN ifecha date,
    IN isaldo_apertura numeric,
    IN icomentarios character varying,
    IN iusuario integer,
    OUT id_corte integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-03
-- Descripcion General: Agregar registro a cortes
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_corte_nuevo integer;
corte_actual RECORD;
diferencia_corte numeric(10,2);
saldo_cierre_actual numeric(10,2);
dif_corte_nuevo numeric(10,2);



BEGIN
  --Variables de salida
  id_corte   := 0;
  flag       := false;
  mensaje    := 'Error al guardar corte ';


  IF (SELECT count(*) FROM cortes WHERE fecha=ifecha) > 0 THEN
  mensaje    := 'Error, ya hay un corte del dia '||ifecha;
  RETURN NEXT;
  RETURN;
  END IF;

  --Obtener información corte actual
  SELECT z.id_corte,z.estado INTO corte_actual FROM obtener_corte_actual() AS z;

  --Verificar que el corte no tenga un estatus CERRADO
  IF corte_actual.estado = 'C' THEN

    --Se inserta el corte
    INSERT INTO cortes(id_usuario,saldo_apertura,fecha,estatus) 
    VALUES(iusuario,isaldo_apertura,ifecha,'A')
    RETURNING id INTO id_corte_nuevo;
  
    --Obtener el saldo actual del corte
    SELECT saldo_cierre INTO saldo_cierre_actual FROM cortes WHERE id=corte_actual.id_corte;
    
    diferencia_corte := isaldo_apertura - saldo_cierre_actual;

    --Generar cargo por diferencia en apertura
    IF diferencia_corte  > 0 THEN

      INSERT INTO incidencias_apertura_corte(id_corte,id_usuario,motivo,movimiento,importe) 
      VALUES(corte_actual.id_corte,iusuario,icomentarios,'A',diferencia_corte);


    ELSEIF diferencia_corte < 0 THEN

      dif_corte_nuevo:= diferencia_corte * -1;
      INSERT INTO incidencias_apertura_corte(id_corte,id_usuario,motivo,movimiento,importe) 
      VALUES(corte_actual.id_corte,iusuario,icomentarios,'C',dif_corte_nuevo);

    END IF;

  ELSE
    --Se inserta el corte
    INSERT INTO cortes(id_usuario,saldo_apertura,fecha,estatus) 
    VALUES(iusuario,isaldo_apertura,ifecha,'A')
    RETURNING id INTO id_corte_nuevo;
    
  END IF;

  --Insertar log
  INSERT INTO logs_cortes(id_corte,id_usuario,descripcion,tipo)
  VALUES (id_corte_nuevo,iusuario,'Creo el corte ID: '||id_corte_nuevo||' con un saldo de apertura de: $'||isaldo_apertura,'C');

  id_corte   := id_corte_nuevo;
  flag       := true;
  mensaje    := 'Se guardo el corte correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  id_corte   := 0;
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;


CREATE OR REPLACE FUNCTION public.obtener_corte_actual(
    OUT fecha_corte text,
    OUT id_corte integer,
    OUT estado character,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-29
-- Descripcion General: Obtener fecha actual y saldo ultimo corte
*****************************************************************************/ 

DECLARE
fecha_servidor text;
fecha_corte_actual text;
id_corte_actual integer;

BEGIN

  SELECT (now() AT TIME ZONE 'America/Mazatlan')::date INTO fecha_servidor;

  --Verificar disponibilidad Bunker
  IF (SELECT count(*) FROM cortes WHERE estatus = 'A') >= 1 THEN
    SELECT COALESCE(max(id),0)::integer INTO id_corte_actual FROM cortes WHERE estatus = 'A' LIMIT 1;
    SELECT fecha::text INTO fecha_corte_actual FROM cortes WHERE id = id_corte_actual;

    IF (fecha_corte_actual::date <  fecha_servidor::date) = true THEN
  fecha_corte  := fecha_corte_actual;
  id_corte     := id_corte_actual;
  estado       := 'D';
  mensaje      := 'Es necesario cerrar el corte actual para generar movimientos, ya que la fecha del corte no corresponde a la fecha actual';
  RETURN NEXT;
    ELSE 
  fecha_corte  := fecha_corte_actual;
  id_corte     := id_corte_actual;
  estado       := 'A';
  mensaje      := 'Actualmente hay un corte activo, es necesario cerrar el corte actual para proceder a crear uno nuevo';
  RETURN NEXT;
    END IF;
    
    
  ELSE
    --Obtener información del ultimo corte
    SELECT COALESCE(max(id),0)::integer INTO id_corte_actual FROM cortes WHERE estatus = 'C';
    SELECT fecha::text INTO fecha_corte_actual  FROM cortes WHERE id = id_corte_actual;
    IF id_corte_actual = 0 THEN
      fecha_corte  := fecha_servidor;
      id_corte     := id_corte_actual;
      estado       := 'N';
      mensaje      := 'Corte inicial';
      RETURN NEXT;
    ELSE
      fecha_corte  := fecha_corte_actual;
      id_corte     := id_corte_actual;
      estado       := 'C';
      mensaje      := 'Se obtuvo información del corte anterior';
      RETURN NEXT;
    END IF;
    
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  fecha_corte := '';
  id_corte     := '';
  estado       := 'E';
  mensaje      := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
  


CREATE OR REPLACE FUNCTION agregar_proveedor_default(
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-29
-- Descripcion General: Agregar registro a tabla proveedores
*****************************************************************************/ 

DECLARE

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al crear cliente ';

  IF (SELECT count(*) FROM proveedores WHERE id=1) = 0 THEN

    
    INSERT INTO proveedores(id_empresa,id_tipo_pago,rfc,nombre_comercial,nombre,email,telefono,calle,
          num_ext,num_int,colonia,cod_postal,localidad,municipio,estado)
    VALUES (0,0,'XAXX010101000','GASTOS SUCURSAL','GASTOS SUCURSAL','DEFAULT@GRUPOALSEDAV.COM','0','CONOCIDO','0','0','CENTRO','80000','CULIACAN','CULIACAN','SINALOA');

    flag       := true;
    mensaje    := 'Se creo el proveedor default';
    RETURN NEXT;

  ELSE
    flag       := false;
    mensaje    := 'Ya existe el proveeor default';
    RETURN NEXT;
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 


CREATE OR REPLACE FUNCTION agregar_solicitud_pago(
    IN iproveedor integer,
    IN iempresa integer,
    IN isucursal integer,
    IN iusuario integer,
    IN iconcepto character varying,
    IN itipo integer,
    IN iimporte numeric,
    IN imetodo integer,
    IN ifecha_vigencia date,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-10
-- Descripcion General: Agregar registro a solicitud_pagos
*****************************************************************************/ 

DECLARE

--Variables de trabajo
id_solicitud_nueva integer;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al guardar solicitud de pago ';

  --Se inserta el pago
  INSERT INTO solicitud_pagos(id_proveedor,id_empresa,id_sucursal,id_usuario,concepto,tipo,metodo,importe_solicitado,fecha_vigencia)
  VALUES(iproveedor,iempresa,isucursal,iusuario,iconcepto,itipo,imetodo,iimporte,ifecha_vigencia)
  RETURNING id INTO id_solicitud_nueva;

  --Se genera log
  INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
  VALUES(id_solicitud_nueva,iusuario,'Solicito un pago de $'||iimporte||' para el día: '||ifecha_vigencia,'S');

  flag       := true;
  mensaje    := 'Se guardo la solicitud de pago correctamente';
  RETURN NEXT;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER; 



CREATE OR REPLACE FUNCTION eliminar_solicitud_pago(
    IN isolicitud integer,
    IN icomentario character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-13
-- Descripcion General: Eliminar solicitud de pago
*****************************************************************************/ 

DECLARE
row_usuario RECORD;
row_solicitud RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al eliminar solicitud de pago ';

  SELECT profile INTO row_usuario FROM users WHERE id=iusuario LIMIT 1;
  SELECT estatus INTO row_solicitud FROM solicitud_pagos WHERE id=isolicitud LIMIT 1;

  IF (row_usuario.profile IN ('admin','director')) THEN
    UPDATE solicitud_pagos SET estatus='B' WHERE id=isolicitud;

    --Se genera log
    INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
    VALUES(isolicitud,iusuario,'Elimino la solicitud de pago, por el siguiente motivo: '||icomentario,'B');

    flag       := true;
    mensaje    := 'Se elimino correctamente la solicitud de pago';
    RETURN NEXT;
  ELSE
    IF (row_solicitud.estatus <> 'S') THEN
      flag       := false;
      mensaje    := 'No es posible eliminar solicitud de pago';
      RETURN NEXT;

    ELSE
      UPDATE solicitud_pagos SET estatus='B' WHERE id=isolicitud;

      --Se genera log
      INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
      VALUES(isolicitud,iusuario,'Elimino la solicitud de pago, por el siguiente motivo: '||icomentario,'B');
      
      flag       := true;
      mensaje    := 'Se elimino correctamente la solicitud';
      RETURN NEXT;
    END IF;
  END IF;

  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;



CREATE OR REPLACE FUNCTION autorizar_solicitud_pago(
    IN ipago integer,
    IN imonto numeric,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-12-10
-- Descripcion General: Autorizar solicitud pago
*****************************************************************************/ 

DECLARE
row_usuario RECORD;
row_pago RECORD;
hora_actual timestamp without time zone;
res_mov RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al autorizar pago ';

  SELECT profile INTO row_usuario FROM users WHERE id=iusuario LIMIT 1;

  SELECT estatus,importe_solicitado,id_empresa,id_sucursal,id_proveedor,concepto,metodo,tipo,id_usuario 
  INTO row_pago 
  FROM solicitud_pagos WHERE id=ipago LIMIT 1;

  SELECT now() at time zone 'America/Mazatlan' INTO hora_actual;

  IF (row_usuario.profile IN ('admin','director')) THEN

    IF imonto > row_pago.importe_solicitado THEN
      flag       := false;
      mensaje    := 'El importe autorizado es mayor al solicitado, favor de corregirlo';
      RETURN NEXT;
    ELSE
    
      SELECT z.id_movimiento,z.flag,z.mensaje 
      INTO res_mov 
      FROM agregar_movimiento(row_pago.id_empresa,row_pago.id_sucursal,
      row_pago.id_proveedor,row_pago.concepto,imonto,row_pago.metodo,
      row_pago.tipo,'C',row_pago.id_usuario) AS z;

      IF res_mov.flag = true THEN

        --Actualizar movimiento a PENDIENTE POR APLICAR
        UPDATE movimientos SET estatus='P' WHERE id=res_mov.id_movimiento;

        UPDATE solicitud_pagos 
        SET id_movimiento=res_mov.id_movimiento, estatus='A', importe_autorizado=imonto, fecha_autorizado=hora_actual 
        WHERE id=ipago;

        --Se genera log
        INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
        VALUES(ipago,iusuario,'Autorizo el pago por: $'||imonto,'A');
        
        flag       := true;
        mensaje    := 'Se autorizo el pago correctamente';
        RETURN NEXT;

      ELSE
        flag       := false;
        mensaje    := res_mov.mensaje;
        RETURN NEXT;
      END IF;
      
    END IF;
  ELSE
    flag       := false;
    mensaje    := 'No cuentas con permisos para autorizar el pago';
    RETURN NEXT;
  END IF;


  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
    flag       := false;
    mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;




  CREATE OR REPLACE FUNCTION rechazar_solicitud_pago(
    IN ipago integer,
    IN icomentario character varying,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-13
-- Descripcion General: Rechazar pago
*****************************************************************************/ 

DECLARE
row_usuario RECORD;
row_pago RECORD;

BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al rechazar solicitud de pago ';

  SELECT profile INTO row_usuario FROM users WHERE id=iusuario LIMIT 1;
  SELECT estatus,importe_solicitado INTO row_pago FROM pagos WHERE id=ipago LIMIT 1;
  
  IF (row_usuario.profile IN ('admin','director')) THEN
   UPDATE solicitud_pagos SET estatus='R',motivo_rechazo=icomentario WHERE id=ipago;

   --Se genera log
   INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
   VALUES(ipago,iusuario,'Rechazo la solicitud de pago, por el siguiente motivo: '||icomentario,'R');

   flag       := true;
   mensaje    := 'Se rechazo la solicitud de pago correctamente';
   RETURN NEXT;
  ELSE
    flag       := false;
    mensaje    := 'No cuentas con permisos para rechazar la solicitud';
    RETURN NEXT;
  END IF;

  
  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;



CREATE OR REPLACE FUNCTION capturar_pago(
    IN ipago integer,
    IN imonto numeric,
    IN iusuario integer,
    OUT flag boolean,
    OUT mensaje character varying)
  RETURNS SETOF record AS
$BODY$

/***************************************************************************** 
-- Elaboro :  Cesar Giovanny Perez Graciano
-- Fecha : 2018-11-13
-- Descripcion General: Capturar pago
*****************************************************************************/ 

DECLARE
row_usuario RECORD;
row_pago RECORD;
hora_actual timestamp without time zone; 


BEGIN
  --Variables de salida
  flag       := false;
  mensaje    := 'Error al capturar pago ';

  ---SELECT profile INTO row_usuario FROM users WHERE id=iusuario LIMIT 1;
  SELECT estatus,importe_autorizado,id_movimiento INTO row_pago FROM solicitud_pagos WHERE id=ipago LIMIT 1;
  SELECT now() at time zone 'America/Mazatlan' INTO hora_actual;

  IF imonto > row_pago.importe_autorizado THEN
    flag       := false;
    mensaje    := 'El importe pagado es mayor al autorizado, favor de corregirlo';
    RETURN NEXT;
  ELSE
    UPDATE solicitud_pagos SET estatus='P',importe_pagado=imonto,fecha_pago=hora_actual WHERE id=ipago;

    UPDATE movimientos SET estatus='R' WHERE id=row_pago.id_movimiento;

    --Se genera log
    INSERT INTO logs_solicitud_pagos(id_solicitud,id_usuario,descripcion,tipo) 
    VALUES(ipago,iusuario,'Capturo el pago por: $'||imonto,'P');

    flag       := true;
    mensaje    := 'Se capturo el pago correctamente';
    RETURN NEXT;  
  END IF;

  EXCEPTION WHEN OTHERS THEN --Recibe todas las excepciones 
  flag       := false;
  mensaje    := 'DETALLES: ' || SQLERRM;  
  RETURN NEXT; 
End
 $BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER;

