/*
	
	DROP TABLE logs_cortes;
	DROP TABLE logs_movimientos;
	DROP TABLE logs_solicitud_pagos;
	DROP TABLE incidencias_apertura_corte;
	DROP TABLE incidencias_cierre_corte;
	DROP TABLE solicitud_pagos;
	DROP TABLE movimientos;
	DROP TABLE logs_tipos_pagos;
	DROP TABLE cat_tipos_pagos;
	DROP TABLE cat_metodos_pago;
	DROP TABLE cortes;
	DROP TABLE proveedores;
*/

CREATE TABLE cat_tipos_pagos
(
	id serial PRIMARY KEY,
	nombre character varying(300),
	visible boolean DEFAULT true,
	flag boolean DEFAULT true,
	movimiento character(1) NOT NULL DEFAULT 'C',
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('GASTOS EN GENERAL','C',true);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('FALTANTE','C',false);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('SOBRANTE','C',false);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('INSUMOS','C',true);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('COMPRAS DE TECNOLOGIA','C',true);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('RETIRO SUCURSALES','A',true);
INSERT INTO cat_tipos_pagos(nombre,movimiento,visible) VALUES('NOMINA','C',true);

CREATE TABLE logs_tipos_pagos (
  id serial PRIMARY KEY NOT NULL,
  id_tipo integer REFERENCES cat_tipos_pagos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE proveedores
(
	id serial PRIMARY KEY,
	id_empresa integer NOT NULL DEFAULT 0,
	id_tipo_pago integer NOT NULL DEFAULT 0,
	rfc character(15) NOT NULL,
	nombre character varying(100) NOT NULL,
	nombre_comercial character varying(100) NOT NULL,
	email character varying(100) NOT NULL,
	telefono character varying(10) NOT NULL,
	calle character varying(100) NOT NULL,
	num_ext integer NOT NULL,
	num_int integer DEFAULT 0,
	colonia character varying(100) NOT NULL,
	cod_postal character varying(10) NOT NULL,
	localidad character varying(50) NOT NULL,
	municipio character varying(50) NOT NULL,
	estado character varying(50) NOT NULL,
	credito numeric(10,2) DEFAULT 0.00,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now()),
	flag boolean DEFAULT true
);

/*
	Estatus:
	A - Apertura
	C - Cierre
	D - Cancelado
	E - Eliminado
*/
CREATE TABLE cortes
(
	id serial PRIMARY KEY NOT NULL,
	id_usuario integer REFERENCES users(id) NOT NULL,
	saldo_apertura numeric(10,2) NOT NULL,
	saldo_cierre numeric(10,2),
	diferencia numeric(10,2),
	saldo_final numeric(10,2),
	estatus character(1) NOT NULL DEFAULT 'A',
	fecha date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE incidencias_apertura_corte
(
	id serial PRIMARY KEY NOT NULL,
	id_corte integer REFERENCES cortes(id),
	id_usuario integer REFERENCES users(id),
	motivo character varying(300) NOT NULL,
	movimiento character(1) NOT NULL,
	importe numeric(10,2) NOT NULL,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


CREATE TABLE incidencias_cierre_corte
(
	id serial PRIMARY KEY NOT NULL,
	id_corte integer REFERENCES cortes(id),
	id_usuario integer REFERENCES users(id),
	motivo character varying(300) NOT NULL,
	movimiento character(1) NOT NULL,
	importe numeric(10,2) NOT NULL,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);


/* CRUD */ 
CREATE TABLE logs_cortes
(
	id serial PRIMARY KEY NOT NULL,
	id_corte integer REFERENCES cortes(id),
	id_usuario integer REFERENCES users(id),
	descripcion character varying(300) NOT NULL,
	tipo character(1) NOT NULL,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE cat_metodos_pago
(
	id serial PRIMARY KEY,
	nombre character varying(300),
	visible boolean DEFAULT true,
	flag boolean DEFAULT true,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

INSERT INTO cat_metodos_pago(nombre) VALUES('EFECTIVO');
INSERT INTO cat_metodos_pago(nombre) VALUES('TARJETA DE DEBITO');
INSERT INTO cat_metodos_pago(nombre) VALUES('TARJETA DE CREDITO');
INSERT INTO cat_metodos_pago(nombre) VALUES('CHEQUE');


/*
	Estatus:
	R - Realizado
	P - Pendiente de Aplicar
	C - Cancelado
	E - Eliminado

	Movimientos:
	C - Cargo
	A - Abono

	Metodo:

*/

CREATE TABLE movimientos
(
	id serial PRIMARY KEY NOT NULL,
	id_corte integer REFERENCES cortes(id) NOT NULL,
	id_proveedor integer NOT NULL DEFAULT 0,
	id_empresa integer NOT NULL DEFAULT 0,
	id_sucursal integer NOT NULL DEFAULT 0,
	id_usuario integer REFERENCES users(id) NOT NULL,
	concepto character varying(300) NOT NULL,
	importe numeric(10,2) NOT NULL,
	metodo integer REFERENCES cat_metodos_pago(id) NOT NULL,
	tipo integer REFERENCES cat_tipos_pagos(id) NOT NULL,
	movimiento character(1) NOT NULL,
	estatus character(1) NOT NULL DEFAULT 'R',
	fecha date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

/* CRUD */ 
CREATE TABLE logs_movimientos
(
	id serial PRIMARY KEY NOT NULL,
	id_movimiento integer REFERENCES movimientos(id),
	id_usuario integer REFERENCES users(id),
	descripcion character varying(300) NOT NULL,
	tipo character(1) NOT NULL,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

/*
	Estatus: 
	S - Solicitado
	R - Rechazado
	B - Borrado
	A - Autorizado
	P - Pagado

	Atrasado por direccion - Cuando estatus = 'S' y la fecha actual es mayor a fecha_vigencia;
	Atrasado por falta de pago - Cuando estatus = 'A' y la fecha actual es mayor a fecha_pago;
*/


CREATE TABLE solicitud_pagos
(
	id serial PRIMARY KEY NOT NULL,
	id_proveedor integer REFERENCES proveedores(id) NOT NULL,
	id_movimiento integer REFERENCES movimientos(id),
	id_empresa integer REFERENCES empresas(id) NOT NULL,
	id_sucursal integer REFERENCES sucursales(id) NOT NULL,
	id_usuario integer REFERENCES users(id) NOT NULL,
	concepto character varying(300) NOT NULL,
	tipo integer REFERENCES cat_tipos_pagos(id) NOT NULL,
	metodo integer REFERENCES cat_metodos_pago(id) NOT NULL,
	importe_solicitado numeric(10,2) NOT NULL,
	fecha_vigencia date NOT NULL, 
	importe_autorizado numeric(10,2),
	fecha_autorizado timestamp without time zone,
	importe_pagado numeric(10,2),
	fecha_pago  timestamp without time zone, 
	estatus character(1) NOT NULL DEFAULT 'S',
	motivo_rechazo character varying(300),
	fecha_registro date NOT NULL DEFAULT timezone('America/Mazatlan'::text, now())::date,
	registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);

CREATE TABLE logs_solicitud_pagos (
  id serial PRIMARY KEY NOT NULL,
  id_solicitud integer REFERENCES solicitud_pagos(id),
  id_usuario integer REFERENCES users(id),
  descripcion character varying(300) NOT NULL,
  tipo character(1) NOT NULL,
  registro timestamp without time zone DEFAULT timezone('America/Mazatlan'::text, now())
);
